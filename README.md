# sxmotify

Rust library for supporting [sxmo](https://sxmo.org/) notifications.

## Features

- [x] Create, modify, and delete notifications.
- [x] Run arbitrary commands on notification click.
- [x] Clear notifications on file being touched.
- [x] Notifications namespace, including clearing all notifications in that namespace.

## Usage

Slap `sxmotify = "~0.1"` in your `Cargo.toml` file.

## Licence

Everything in this repo that can be, is available under the GPL licence, v3 or later, as in the COPYING file.
