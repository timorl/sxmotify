//! Create, modify, and delete sxmo notifications.
//!
//! Can be used directly or in namespace mode, where it allows for cleaning up all notifications in a namespace.
use directories::BaseDirs;
use std::path;

mod basic;
mod namespace;

pub use basic::{clear_notification, set_notification};
pub use namespace::Namespace;

fn notification_directory() -> path::PathBuf {
    let mut result = BaseDirs::new().unwrap().data_dir().to_path_buf();
    result.push("sxmo/notifications/");
    result
}
